
const tf = require("@tensorflow/tfjs-node"),
    fs = require("fs");

// Define a model for linear regression.
tf.disableDeprecationWarnings();

fs.readFile("./code1.txt", "utf8", function (err, data) {
    if (err) throw err;
    var content = eval(data),
        xxs = content[0],
        yys = content[1],
        xs = tf.tensor(xxs),
        ys = tf.tensor(yys),
        value = [];
    console.log("input shape = " + xs.shape);
    console.log("output shape = " + ys.shape);
    async function run(z, zz) {
        var model = tf.sequential(),
            activation = "relu";
        if (zz == 1) {
            activation = "sigmoid";
        }
        if (z == 1) {
            model.add(tf.layers.dense({
                units: ys.shape[1],
                inputShape: [xs.shape[1]],
                learning_rate: 0.1,
                activation: activation
            }));
        }
        else if (z == 2) {
            model.add(tf.layers.dense({
                units: ys.shape[1],
                inputShape: [xs.shape[1]],
                learning_rate: 0.1,
                activation: activation
            }));
            model.add(tf.layers.dense({
                units: ys.shape[1],
                inputShape: [ys.shape[1]],
                learning_rate: 0.1
            }));
        }
        // Prepare the model for training: Specify the loss and the optimizer.
        model.compile({ loss: 'meanSquaredError', optimizer: 'sgd' });
        var fullInfo = [];
        await model.fit(xs, ys, {
            batchSize: 4,
            epochs: 100,
            callbacks: {
                onEpochEnd: async (epoch, log) => {
                    //console.log("Epoch: " + epoch + " loss: " + log.loss);
                    fullInfo.push([epoch, log.loss]);
                }
            }
        }).then(function () {
            console.log("*** FINISHED LEARNING ***");
            for (var i = 0; i < xxs.length; i++) {
                var tens = tf.tensor([xxs[i]]);
                //check.print(summarize=xxs[i].length);
                console.log("------------------------------------------------");
                console.log("Input: " + xxs[i]);
                console.log("Output: " + tens.dataSync());
                console.log("Expected: " + yys[i]);
                // console.log("Predicted: " + out);
            }
            fs.writeFile("info(1)" + z + "layers" + activation + ".csv", fullInfo.join("\n"), function (err) {
                if (err) throw err;
                console.log("Wrote info" + z + "layers" + activation + ".csv" + " to disk.");
            });
        });
    }
    async function r() {
        for (var z = 1; z <= 2; z++) {
            for (var zz = 1; zz <= 2; zz++) {
                await run(z, zz);
            }
        }
    }
    r();
});