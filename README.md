## How to Use
This version uses two months as an input, rather than one.
This is a program designed to predict the severity of the flu in the state. Unfortunately it's been found that it is... a little harder than it looks. The severity file, from the CDC, is very difficult to parse. As a result, there are lots of different files, which must be run in a specific order to return the correct data.

1. **code.js** - The final file. This produces **info.csv**.
2. **formatted.js** - Creates **code1.txt**, to use in **code.js**.
3. **formatted.csv** - Where everything begins. It's the file from the CDC, except stripped down to all the stuff we need.
4. **formatted1.csv** - Formats **formatted.csv**, which gives us an array of the severities, organized by the dates.
5. **info.csv** - The list of all the losses.
6. **index.js** - Creates **formatted1.csv**.
7. **codeComparing.js** - Creates all the info files, which can be compared using other tools.
8. **ActualVSPredicted.js** - Creates **Compare.csv**, which contains all the predictions versus the normal values.
9. **UI.js** - Predicts the flu for next week and creates a UI with index.html.

If you wanted to run this....
1. Run **index.js**.
2. Run **formatted.js**.
3. Run **code.js**.
And you're done!