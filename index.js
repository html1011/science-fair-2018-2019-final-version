/*
Formatted.csv: compiles all useful information into a useable form.
Formatted1.csv: Creates a table to plug into NN.
*/
const tf = require("@tensorflow/tfjs"),
    fs = require("fs");

// Define a model for linear regression.
tf.disableDeprecationWarnings();
// Array.prototype.transform = function () {
//     return this.map(function (val) {
//         return [val];
//     })
// }
var model = tf.sequential();

//var ,
//   ys = tf.tensor([[1.0, 1.0], [2.0, 3.0], [3.0, 1.0], [4.0, 7.0]]);


var xs = tf.tensor([[1.0, 1.0], [2.0, 3.0], [3.0, 1.0], [4.0, 7.0]]),
    ys = tf.tensor([[1.0, 2.0], [2.0, 5.0], [3.0, 4.0], [4.0, 11.0]]);




console.log("input shape = " + xs.shape + "\noutput shape = " + ys.shape);

model.add(tf.layers.dense({
    units: ys.shape[1],
    inputShape: [xs.shape[1]],
    learning_rate: 0.1
}));

// Prepare the model for training: Specify the loss and the optimizer.
model.compile({ loss: 'meanSquaredError', optimizer: 'sgd' });


model.fit(xs, ys, {
    batchSize: 4,
    epochs: 100
}).then(function () {
    console.log("*** FINISHED LEARNING ***");

    var tens = tf.tensor([[3, 6]]);
    var check = model.predict(tens);

    console.log("Finished training: " + check);
});



// Train the model using the data.
async function run() {
    var i = 0,
        h = await model.fit(xs, ys, {
            batchSize: 4,
            epochs: 10
        });
    while (h.history.loss[0] > 0.0005 && i < 1000) {
        h = await model.fit(xs, ys, {
            batchSize: 4,
            epochs: 10
        });
        console.log("Loss after Epoch " + i + " : " + h.history.loss[0]);
        i++;
    }
    console.log("Finished training: " + model.predict(tf.tensor([1], [0], [99], [0])));
}
//run();

// Object.prototype.turnToArray = function () {
//     var val = Object.getOwnPropertyNames(this),
//         answer = [];
//     for (var i = 0; i < val.length; i++) {
//         answer.push([val[i], this[val[i]]]);
//     }
//     return answer;
// }
// if (!fs.existsSync("./Formatted1.csv")) {
//     // If file not created, we create it.
//     fs.readFile("./Formatted.csv", "utf8", function (err, data) {
//         if (err) throw err;
//         let content = data.toString().split("\n");
//         for (var i = 0; i < content.length; i++) {
//             content[i] = content[i].split(",");
//         }
//         // Now we have properly formatted our data...
//         // Okay. What we need to do is set up two arrays, one formatted by date and the other by states.
//         let stateBow = [],
//             dateBow = [],
//             SB = [],
//             DB = [],
//             content1 = content.slice();
//         content = content.sort(function (a, b) {
//             return a[0].localeCompare(b[0]);
//         });
//         content1 = content1.sort(function (a, b) {
//             return new Date(a[2]) - new Date(b[2]);
//         });
//         //console.log(content1);
//         while (content.length) {
//             if (content[1]) {
//                 if (content[0][0] !== content[1][0]) {
//                     stateBow.push([content[0][0], JSON.parse(JSON.stringify(SB, null, "  "))]);
//                     SB.length = 0;;
//                 }
//                 else {
//                     SB.push(content[0]);
//                 }
//             }
//             else {
//                 stateBow.push([content[0][0], JSON.parse(JSON.stringify(SB, null, "  "))]);
//                 SB.length = 0;
//             }
//             content.splice(0, 1);
//         }
//         while (content1.length) {
//             if (content1[1]) {
//                 if (content1[0][2] !== content1[1][2]) {
//                     dateBow.push([content1[0][2], JSON.parse(JSON.stringify(DB, null, "  "))]);
//                     DB.length = 0;
//                 }
//                 else {
//                     DB.push(content1[0]);
//                 }
//             }
//             else {
//                 dateBow.push([content1[0][2], JSON.parse(JSON.stringify(DB, null, "  "))]);
//                 DB.length = 0;
//             }
//             content1.splice(0, 1);
//         }
//         // Okay, now we have BOWs for both.
//         /*
//         Sample data:
//         ,Alabama,Alaska,Arizona,Arkansas,California,Colorado,Connecticut,Delaware,District of Columbia,Florida,Georgia,Hawaii,Idaho,Illinois,Indiana,Iowa,Kansas,Kentucky,Louisiana,Maine,Maryland,Massachusetts,Michigan,Minnesota,Mississippi,Missouri,Montana,Nebraska,Nevada,New Hampshire,New Jersey,New Mexico,New York,New York City,North Carolina,North Dakota,Ohio,Oklahoma,Oregon,Pennsylvania,Puerto Rico,Rhode Island,South Carolina,South Dakota,Tennessee,Texas,Utah,Vermont,Virgin Islands,Virginia,Washington,West Virginia,Wisconsin,Wyoming
// Oct-04-2008,1,1,1,6,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0,1,1,1,1,1
// Oct-11-2008,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0,1,1,1,1,1
// Oct-18-2008,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0,1,1,1,1,1
// Oct-25-2008,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,2,1,1,1,1,0,1,1,1,1,1
// Nov-01-2008,3,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,4,1,1,1,1,0,1,1,1,1,1
// Nov-08-2008,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,0,1,1,4,1,1,1,1,0,1,1,1,1,1
// Nov-15-2008,2,1,1,1,1,1,1,1,0,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0,1,1,2,1,1
// Nov-22-2008,3,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,7,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0,2,1,1,1,1
// Nov-29-2008,4,1,1,2,1,1,1,1,0,1,3,1,1,1,1,1,1,1,1,1,2,1,1,1,6,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0,3,1,1,1,1
// Dec-06-2008,5,1,1,1,1,1,1,1,0,1,2,1,1,1,1,1,1,1,1,1,2,1,1,1,6,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0,3,1,1,1,1
// Dec-13-2008,4,1,1,1,1,1,1,1,0,1,2,1,1,1,1,1,1,1,0,1,2,1,1,1,7,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0,3,1,1,1,1
//         */
//         // 1) Create BOW for state & date
//         // 2) Now create the graph!
//         for (var i = 0; i < stateBow.length; i++) {
//             stateBow[i][1] = stateBow[i][1].sort(function (a, b) {
//                 // Order by date
//                 return new Date(a[2]) - new Date(b[2]);
//             });
//         }
//         // So. We're going to go through stateBow. If we have the date, good. If not, use a past date.
//         //console.log(JSON.stringify(dateBow, null, "  "));
//         var answer = [];
//         for (var i = 0; i < dateBow.length; i++) {
//             // Let's check if we have all the states.
//             var date = new Date(dateBow[i][0]);
//             //console.log(date);
//             // dateBow[i][1] is the states, date[i][0] is the date.
//             // Okay... If we don't have all the states, we need to find out WHAT we don't have.
//             answer.push([date]);
//             var ans1 = [];
//             //console.log(dateBow[i]);
//             // dateBow[i][0] is all we care about right now.
//             for (var ii = 0; ii < stateBow.length; ii++) {
//                 var stuffToMessUp = stateBow.slice(),
//                     newestDate = stuffToMessUp[ii][1][0];
//                 for (var z = 0; z < stuffToMessUp[ii][1].length; z++) {
//                     if (Math.abs(date - (new Date(newestDate[2]))) > Math.abs((date - new Date(stuffToMessUp[ii][1][z][2])))) {
//                         // This is our new date.
//                         newestDate = stuffToMessUp[ii][1][z];
//                     }
//                 }
//                 ans1.push(newestDate);
//             }
//             //console.log(ans1);
//             //console.log(date, ans1);
//             answer[answer.length - 1].push(ans1.slice());
//             ans1.length = 0;
//             console.log(Math.round((i / (dateBow.length)) * 100) + "%");
//         }
//         // for (var i = 0; i < answer.length; i++) {
//         //     console.log(answer[i]);
//         // }
//         content = answer;
//         var fullData = [];
//         for (var i = 0; i < content.length; i++) {
//             // Input: [state's severities]
//             // Output: [future severities]
//             var trainingData = {};
//             for (var ii = 0; ii < content[i][1].length; ii++) {
//                 trainingData[content[i][1][ii][0]] = Number(content[i][1][ii][1]);
//             }
//             fullData.push([content[i][0], JSON.parse(JSON.stringify(trainingData))]);
//         }
//         // Normalize data first.
//         for (var i = 0; i < fullData.length; i++) {
//             var list = Object.getOwnPropertyNames(fullData[i][1]);
//             for (var ii = 0; ii < list.length; ii++) {
//                 fullData[i][1][list[ii]] /= 10;
//             }
//         }
//         fs.writeFile("./Formatted1.csv", JSON.stringify(fullData, null, "  "), function (err) {
//             if (err) throw err;
//             console.log("Finished formatting; see Formatted1.csv");
//         });
//     });
// }
// else {
//     fs.readFile("./Formatted1.csv", "utf8", function (err, data) {
//         if (err) throw err;
//         var content = eval(data);
//         //console.log(content);
//         for (var i = 0; i < content.length; i++) {
//             content[i][1] = content[i][1].turnToArray();
//             content[i][1] = content[i][1].map(function (val) {
//                 return val[1];
//             });
//         }
//         content = content.map(function (val) {
//             return val[1];
//         });
//         var xs = [
//         ];
//         var ys = [/* 
//             [1, 0],
//             [0, 1],
//             [0, 1] */
//         ];
//         for (var i = 0; i < content.length - 1; i++) {
//             // Input: content[i]
//             // Output: content[i + 1]
//             xs.push([content[i]]);
//             ys.push([content[i + 1]]);
//         }
//         // Define a model for linear regression.
//         tf.disableDeprecationWarnings();
//         Array.prototype.transform = function () {
//             return this.map(function (val) {
//                 return [val];
//             })
//         }
//         //run();
//         // console.log(xs, ys);
//         //     for (var i = 0; i < content.length - 1; i++) {
//         //         console.log("Input: " + JSON.stringify(content[i], null, "  "));
//         //         const xPredict = tf.tensor2d(content[i]);
//         //         const prediction = net.predict(xPredict);
//         //         prediction.print();
//         //         console.log("Expected: " + JSON.stringify(content[i + 1], null, "  "));
//         //     }
//         // });
//     });
// }