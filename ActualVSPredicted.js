
const tf = require("@tensorflow/tfjs-node"),
    fs = require("fs");

// Define a model for linear regression.
tf.disableDeprecationWarnings();

fs.readFile("./code1.txt", "utf8", function (err, data) {
    if (err) throw err;
    var content = eval(data),
        xxs = content[0],
        yys = content[1],
        xs = tf.tensor(xxs),
        ys = tf.tensor(yys);
    console.log("input shape = " + xs.shape);
    console.log("output shape = " + ys.shape);
    async function run() {
        var model = tf.sequential(),
            activation = "relu";
        model.add(tf.layers.dense({
            units: ys.shape[1],
            inputShape: [xs.shape[1]],
            learning_rate: 0.01,
            activation: activation
        }));
        model.add(tf.layers.dense({
            units: ys.shape[1],
            inputShape: [ys.shape[1]],
            learning_rate: 0.01
        }));
        model.add(tf.layers.dense({
            units: ys.shape[1],
            inputShape: [ys.shape[1]],
            learning_rate: 0.1
        }));
        // Prepare the model for training: Specify the loss and the optimizer.
        model.compile({ loss: 'meanSquaredError', optimizer: "adam" });
        var fullInfo = [];
        await model.fit(xs, ys, {
            batchSize: 4,
            epochs: 100,
            callbacks: {
                onEpochEnd: async (epoch, log) => {
                    //console.log("Epoch: " + epoch + " loss: " + log.loss);
                    fullInfo.push([epoch, log.loss]);
                }
            }
        }).then(function () {
            console.log("*** FINISHED LEARNING ***");
            var fullData = [];
            for (var i = 0; i < xxs.length; i++) {
                var tens = tf.tensor([xxs[i]]),
                    info = [];
                //check.print(summarize=xxs[i].length);
                //console.log("Input: " + xxs[i]);
                //console.log("Output: " + tens.dataSync());
                //console.log("Expected: " + yys[i]);
                var answer = model.predict(tens).arraySync()[0];
                for (var ii = 0; ii < answer.length; ii++) {
                    info.push([yys[i][ii], answer[ii]]);
                    //console.log([yys[i][ii], tens.dataSync()[ii]]);
                }
                fullData.push(info);
            }
            // We want to label our data, so we need to go back into formatted.csv.
            fs.readFile("./Formatted1.csv", function (err, data) {
                if (err) throw err;
                var content = JSON.parse(data),
                    list = [],
                    loopIn = Object.getOwnPropertyNames(content[0][1]);
                // Get list
                for (var i = 0; i < loopIn.length; i++) {
                    list.push("Actual Values" + loopIn[i], "Predicted Values " + loopIn[i]);
                }
                fullData.splice(0, 0, list);
                fs.writeFile("./Compare.csv", fullData.join("\n"), function (err) {
                    if (err) throw err;
                });
            });
        });
    }
    async function r() {
        await run();
    }
    r();
});