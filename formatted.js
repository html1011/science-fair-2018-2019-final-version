var fs = require("fs"),
    calc = [];
Object.prototype.turnToArray = function () {
    var val = Object.getOwnPropertyNames(this),
        answer = [];
    for (var i = 0; i < val.length; i++) {
        answer.push([val[i], this[val[i]]]);
    }
    return answer;
}
fs.readFile("./Formatted1.csv", "utf8", function (err, val) {
    if (err) throw err;
    var content = eval(val),
        ans = [];
    for (var i = 0; i < content.length; i++) {
        // Convert this to an array
        var calc1 = content[i][1].turnToArray();
        ans.push(calc1.map(function (val) {
            return val[1];
        }));
    }
    var inputs = [],
        outputs = [];
    for (var i = 0; i < ans.length - 2; i++) {
        //var anstmp = [ans[i], ans[i + 1]];
        inputs.push(ans[i].concat(ans[i + 1]));
        outputs.push(ans[i + 2]);
    }
    fs.writeFile("./code1.txt", `${JSON.stringify([inputs, outputs], null, "  ")}`, function (err) {
        if (err) throw err;
        console.log("Finished!");
    });
})